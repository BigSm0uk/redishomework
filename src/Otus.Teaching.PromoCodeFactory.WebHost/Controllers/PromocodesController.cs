﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly PromoCodeRepository _promoCodeRepository;
        private readonly PreferenceRepository _preferenceRepository;
        private readonly CustomerRepository _customerRepository;
        private readonly EmployRepository _employRepository;

        public PromocodesController(PromoCodeRepository prm, PreferenceRepository preferenceRepository, CustomerRepository customerRepository, EmployRepository employRepository)
        {
            _promoCodeRepository = prm;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employRepository = employRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var promoCodesModelList = promoCodes.Select(x => 
                new PromoCodeShortResponse()
                {
                 Id   = x.Id,
                 Code = x.Code,
                 //ServiceInfo = x.ServiceInfo,
                 BeginDate = x.BeginDate.ToString(),
                 EndDate = x.EndDate.ToString(),
                 PartnerName = x.PartnerName,
                }).ToList();

            return promoCodesModelList;
        }
        
        /// <summary>
        /// Получить промокод по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<PromoCodeShortResponse> GetPromocodesByIdAsync(Guid id)
        {
            var promoCodes = await _promoCodeRepository.GetByIdAsync(id);

            var promoCodesModel = new PromoCodeShortResponse()
            {
                Id = promoCodes.Id,
                Code = promoCodes.Code,
                //ServiceInfo = promoCodes.ServiceInfo,
                BeginDate = promoCodes.BeginDate.ToString(),
                EndDate = promoCodes.EndDate.ToString(),
                PartnerName = promoCodes.PartnerName,
            };
 
            return promoCodesModel;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var promocode = new PromoCode();
            promocode.Id= Guid.NewGuid();
            promocode.Code = request.PromoCode;
            //promocode.ServiceInfo = request.ServiceInfo;
            promocode.PartnerName = request.PartnerName;
            promocode.Preference = _preferenceRepository.GetByIdAsync(Guid.Parse(request.Preference)).Result;
            var ids = request.Customers.Select(varibale => Guid.Parse(varibale)).ToList();
            promocode.Customer = _customerRepository.GetRangeByIdsAsync(ids).Result as List<Customer>;
            promocode.PartnerManager = _employRepository.GetByIdWithRoleAsync(Guid.Parse(request.Employ)).Result ;
            
            await _promoCodeRepository.CreateAsync(promocode);
            
            return Ok();
        }
        /// <summary>
        /// Обновить промокод
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]    
        public async Task<IActionResult> EditPromoCodeAsync(Guid id, GivePromoCodeRequest request)
        {
            var promocode = await _promoCodeRepository.GetByIdAsync(id);
            if (promocode == null)
                return NotFound();
            if(request == null)
                return BadRequest();
            promocode.Code = request.PromoCode;
            //promocode.ServiceInfo = request.ServiceInfo;
            promocode.PartnerName = request.PartnerName;
            promocode.Preference = _preferenceRepository.GetByIdAsync(Guid.Parse(request.Preference)).Result;
            var ids = request.Customers.Select(varibale => Guid.Parse(varibale)).ToList();
            promocode.Customer = _customerRepository.GetRangeByIdsAsync(ids).Result as List<Customer>;
            promocode.PartnerManager = _employRepository.GetByIdWithRoleAsync(Guid.Parse(request.Employ)).Result ;
            
            await _promoCodeRepository.UpdateByIdAsync(id, promocode);
            
            return Ok();
        }
        /// <summary>
        /// Удалить промокод
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeletePromoCode(Guid id)
        {
            var promoCode = await _promoCodeRepository.GetByIdAsync(id);
            await _promoCodeRepository.DeleteItem(promoCode);
            return Ok();
        }
    }
}