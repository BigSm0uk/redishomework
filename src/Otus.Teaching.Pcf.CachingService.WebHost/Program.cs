using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.CachingServce.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.CachingService.DataAccess;
using Otus.Teaching.Pcf.CachingService.DataAccess.Data;
using Otus.Teaching.Pcf.CachingService.DataAccess.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<DataContext>(x =>
{
    //x.UseSqlite("Filename=PromocodeFactoryReceivingFromPartnerDb.sqlite");
   x.UseNpgsql(builder.Configuration.GetConnectionString("CachingPreferenceServiceDb")!);
});
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();
builder.Services.AddScoped<PreferenceRepository>();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = "localhost";
    options.InstanceName = "SampleInstance";
});

var app = builder.Build();

// app.Services.GetService<EfDbInitializer>()?.InitializeDb();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

