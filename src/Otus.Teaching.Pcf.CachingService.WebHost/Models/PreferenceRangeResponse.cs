﻿namespace CachingService.Models;

public class PreferenceRangeResponse
{
    public List<Guid> ids { get; set; }
}