﻿using System.Text.Json;
using CachingService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.CachingServce.Core.Domain;
using Otus.Teaching.Pcf.CachingService.DataAccess.Repositories;

namespace CachingService.Controllers;
[ApiController]
[Route("api/v1/[controller]")]
public class GivePreferenceController : Controller
{
    private readonly PreferenceRepository _preferencesRepository;
    private readonly IDistributedCache _distributedCache;

    public GivePreferenceController(PreferenceRepository preferencesRepository, IDistributedCache distributedCache) =>
        (_preferencesRepository, _distributedCache) = (preferencesRepository, distributedCache);

    [HttpGet("{id:guid}")]
    public async Task<Preference> GetPreference(Guid id)
    {
        Preference? entity = null!;
        var preferenceString = _distributedCache.GetStringAsync(id.ToString()).Result;
        if(preferenceString !=null) entity = JsonSerializer.Deserialize<Preference>(preferenceString);
        if (entity == null)
        {
            entity = _preferencesRepository.GetByIdAsync(id).Result;
            preferenceString = JsonSerializer.Serialize(entity);
            await _distributedCache.SetStringAsync(entity.Id.ToString(), preferenceString,
                new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1)
                });
        }
        return entity;
    }

    [HttpGet]
    public Task<List<Preference>> GetAllPreference()
    {
        var entities =_preferencesRepository.GetAllAsync();
        return entities;
    }
    [HttpGet("Range")]
    public Task<List<Preference>> GetRangeByIdsPreference([FromQuery]PreferenceRangeResponse preferences)
    {
        var entities = _preferencesRepository.GetRangeByIdsAsync(preferences.ids);
        return entities;
    }
    
}