﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class CachingPreferenceGateway : ICachingPreferenceGateway
    {
        private readonly HttpClient _httpClient;

        public CachingPreferenceGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Preference> GivePreferenceByIdAsync(Guid preferenceId)
        {
            var response = await _httpClient.GetStringAsync($"api/v1/GivePreference/{preferenceId}");
            return JsonConvert.DeserializeObject<Preference>(response);
        }

        public async Task<List<Preference>> GiveAllPreferenceAsync()
        {
            var responses = await _httpClient.GetStringAsync("api/v1/GivePreference");
            return JsonConvert.DeserializeObject<List<Preference>>(responses);
        }

        public async Task<IEnumerable<Preference>> GetRangeByIdsAsync(List<Guid> requestPreferenceIds)
        {
            var requesUri = "api/v1/GivePreference/Range?";
            if (requestPreferenceIds.Count != 0)
            {
                requesUri += $"ids={requestPreferenceIds[0]}";
                requesUri = requestPreferenceIds.Aggregate(requesUri, (current, variable) => current + ("&ids=" + variable));
            }
            var responses = await _httpClient.GetStringAsync(requesUri);
            return JsonConvert.DeserializeObject<List<Preference>>(responses);
        }
    }
}