﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface ICachingPreferenceGateway
    {
        Task<Preference> GivePreferenceByIdAsync(Guid preferenceId);
        Task<List<Preference>> GiveAllPreferenceAsync();
        Task<IEnumerable<Preference>> GetRangeByIdsAsync(List<Guid> requestPreferenceIds);
    }
}