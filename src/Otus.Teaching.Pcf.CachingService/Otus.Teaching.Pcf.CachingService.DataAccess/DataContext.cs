﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.CachingServce.Core.Domain;


namespace Otus.Teaching.Pcf.CachingService.DataAccess;

public class DataContext 
    : DbContext
{
    public virtual DbSet<Preference> Preferences { get; set; } = null!;

    public DataContext()
    {

    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder
            .UseLazyLoadingProxies();

    }

    public DataContext(DbContextOptions<DataContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Preference>().HasKey(u => u.Id);
        base.OnModelCreating(modelBuilder);
    }
}