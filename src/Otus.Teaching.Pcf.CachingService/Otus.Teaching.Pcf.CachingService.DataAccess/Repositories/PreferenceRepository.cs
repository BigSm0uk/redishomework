﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.CachingServce.Core.Domain;

namespace Otus.Teaching.Pcf.CachingService.DataAccess.Repositories;

public class PreferenceRepository : EfRepository<Preference>
{
    public PreferenceRepository(DataContext dataContext) : base(dataContext)
    { }

    public new async Task<Preference> GetByIdAsync(Guid id)
    {
        return (await Task.FromResult(await _dataContext.Preferences.FirstOrDefaultAsync(x => x.Id == id)))!;
    }

    public async Task<List<Preference>> GetAllAsync()
    {
        return await Task.FromResult(await _dataContext.Preferences.ToListAsync());
    }
    
    public async Task<List<Preference>> GetRangeByIdsAsync(List<Guid> ids)
    {
        return  await Task.FromResult(_dataContext.Preferences.Where(x=>ids.Contains(x.Id)).ToList());
    }
}   