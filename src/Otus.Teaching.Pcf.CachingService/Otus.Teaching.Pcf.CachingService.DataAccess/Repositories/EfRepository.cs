﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.CachingServce.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.CachingServce.Core.Domain;

namespace Otus.Teaching.Pcf.CachingService.DataAccess.Repositories;

public class EfRepository<T> : IRepository<T> where T : BaseEntity
{
    protected readonly DataContext _dataContext;

    public EfRepository(DataContext dataContext)
    {
        _dataContext = dataContext;
        
    }
    public async Task<T> GetByIdAsync(Guid id)
    {
        var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

        return entity ?? throw new InvalidOperationException();
    }
}