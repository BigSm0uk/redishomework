﻿namespace Otus.Teaching.Pcf.CachingService.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}