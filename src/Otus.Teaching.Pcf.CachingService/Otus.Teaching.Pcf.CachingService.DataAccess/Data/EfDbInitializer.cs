﻿namespace Otus.Teaching.Pcf.CachingService.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            // _dataContext.Database.EnsureDeleted();
            // _dataContext.Database.EnsureCreated();
            // _dataContext.AddRangeAsync(FakeDataFactory.Preferences);
            // _dataContext.SaveChanges();
            
        }
    }
}