﻿namespace Otus.Teaching.Pcf.CachingServce.Core.Domain;

public class BaseEntity
{
    public Guid Id { get; set; }
}