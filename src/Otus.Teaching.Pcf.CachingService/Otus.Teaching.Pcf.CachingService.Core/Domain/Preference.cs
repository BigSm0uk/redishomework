﻿namespace Otus.Teaching.Pcf.CachingServce.Core.Domain;

public class Preference
    :BaseEntity
{
    public string Name { get; set; }
}