﻿namespace Otus.Teaching.Pcf.CachingServce.Core.Abstractions.Repositories;

public interface IRepository<T>
{
    public Task<T> GetByIdAsync(Guid id);
}