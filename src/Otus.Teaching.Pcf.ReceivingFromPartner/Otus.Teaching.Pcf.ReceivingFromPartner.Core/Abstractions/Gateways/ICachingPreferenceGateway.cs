﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface ICachingPreferenceGateway
    {
        Task<Preference> GivePreferenceByIdAsync(Guid preferenceId);
        Task<List<Preference>> GiveAllPreferenceAsync();

    }
}