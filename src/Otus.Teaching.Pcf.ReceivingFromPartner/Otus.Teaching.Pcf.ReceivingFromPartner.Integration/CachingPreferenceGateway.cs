﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class CachingPreferenceGateway : ICachingPreferenceGateway
    {
        private readonly HttpClient _httpClient;

        public CachingPreferenceGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Preference> GivePreferenceByIdAsync(Guid preferenceId)
        {
            var response = await _httpClient.GetStringAsync($"api/v1/GivePreference/{preferenceId}");
            return JsonConvert.DeserializeObject<Preference>(response);
        }

        public async Task<List<Preference>> GiveAllPreferenceAsync()
        {
            var responses = await _httpClient.GetStringAsync("api/v1/GivePreference");
            return JsonConvert.DeserializeObject<List<Preference>>(responses);
        }
    }
}