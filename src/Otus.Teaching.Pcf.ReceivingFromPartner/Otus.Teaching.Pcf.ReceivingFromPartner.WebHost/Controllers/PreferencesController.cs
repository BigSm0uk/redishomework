﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly ICachingPreferenceGateway _cachingPreferenceGateway;

        public PreferencesController( ICachingPreferenceGateway cachingPreferenceGateway)
        {
            _cachingPreferenceGateway = cachingPreferenceGateway;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _cachingPreferenceGateway.GiveAllPreferenceAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
        }
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesByIdAsync(Guid id )
        {
            var preferences = await _cachingPreferenceGateway.GivePreferenceByIdAsync(id);
            
            return Ok(preferences);
        }
    }
}