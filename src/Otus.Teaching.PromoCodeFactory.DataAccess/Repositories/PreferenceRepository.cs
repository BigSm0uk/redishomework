﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository : EfRepository<Preference>
    {
        public PreferenceRepository(DataContex dataContex) : base(dataContex)
        {
        }

        public Task<IEnumerable<Preference>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return Task.FromResult<IEnumerable<Preference>>(ids.Select(variable => GetByIdAsync(variable).Result).ToList());
        }
    }
}