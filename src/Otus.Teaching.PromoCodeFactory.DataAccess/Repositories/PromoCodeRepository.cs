﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromoCodeRepository: EfRepository<PromoCode>
    {
        public PromoCodeRepository(DataContex dataContex) : base(dataContex)
        {
        }
        public async Task<PromoCode> CreateAsync(PromoCode item)
        {
            await _dataContext.PromoCodes.AddAsync(item);
            await _dataContext.SaveChangesAsync();
            return item;
        }
        public async Task<PromoCode> UpdateByIdAsync(Guid id, PromoCode item)
        {
            _dataContext.PromoCodes.Update(item);
            await _dataContext.SaveChangesAsync();
            return item;
        }
        public async Task DeleteItem(PromoCode item)
        {
            _dataContext.PromoCodes.Remove(item);
            await _dataContext.SaveChangesAsync();
        }
    }
}